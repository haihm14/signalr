﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace SignalR
{
    public class MessageHub : Hub
    {
        private static int count = 0;
        public async Task NewMessage(string user, string msg)
        {
            // await Clients.All.BroadcastMessage();
            await Clients.All.SendAsync("send", msg);
        }

        public override Task OnConnectedAsync()
        {
            count += 1;
            string userId = Context.ConnectionId;
            Console.WriteLine("client connected");
            Clients.All.SendAsync("all-client", count);
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception ex)
        {
            // do the logging here
            count -= 1;
            Clients.All.SendAsync("all-client", count);
            Console.WriteLine("client disconnected");
            return base.OnDisconnectedAsync(ex);
        }
 
    }
}
